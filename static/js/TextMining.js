//stopwords from   https://github.com/6/stopwords-json
//Basic trie with each node containing at most one letter
const SC_MODE_SEARCH = 1;
const SC_MODE_ADD = 2;
const SC_MODE_ADD_DOC = 5;
const S_MODE_BOOLEAN = 3
const S_MODE_NODE = 4;
const M_MODE_INC_DOC_COUNT = 6;
//CLASSES
//TODO REPLACE ARRAYS WITH LINKEDLISTS
class TrieNode{
    constructor(value){
        this.childList = new Array();
        this.value = value;
        this.doc_wordCount = 0;
        this.docCount = 0; //will only be incremented in certain cases.
        this.general_wordCount = 0;
    }

    incrementDocCountChildren(){
        if(this.doc_wordCount > 0)
            this.docCount++;
        let i;
        for(i in this.childList){
            this.childList[i].incrementDocCountChildren();
        }
    }
    /**
    * retourne true si le mot est trouvé dans l'arbre, false sinon.
    * @param {String} string 
    */
    search(string, mode){
        if(string === null)
            throw "Null string not allowed";
        if(string == "")
            return mode === S_MODE_BOOLEAN ? true : this;
        var index = this.searchChild(string.substring(0,1), SC_MODE_SEARCH);
        //in the case child.childsList[0] is null the following code should return false
        if(index < 0)
            return mode === S_MODE_BOOLEAN ? false : null;

        return this.childList[index].search(string.substring(1));
    }

    /**
    * if mode is ADD returns the index at which a string prefixed by firstChar should be inserted.
    * if mode is SEARCH returns the index of firstChar if firstChar is found.
    * but if it isn't found, it returns -1.
    * 
    * HOW TO USE:
    * in ADD mode check equality of the firstChar after this function call:
    *  if it's equal, you can add your word right there.
    *  if it isn't, you need to go deeper.
    * 
    * @param {String} firstChar 
    * @param {SC_MODE_ADD || SC_MODE_SEARCH} mode 
    */
    searchChild(firstChar, mode){
        var i;
        for (i = 0; i<this.childList.length; i++)
            if(this.childList[i].value >= firstChar)
                break;
        if(mode === SC_MODE_ADD)
            return i;
        else if(mode === SC_MODE_SEARCH){
            if(i < this.childList.length && this.childList[i].value === firstChar)
                return i;
            return -1;
        }
    }

    /**
    * returns the node at which the last letter of the word has been inserted
    * if the word was already there, increments count and returns null.
    * @param {*} root 
    * @param {*} string 
    */
    add(string){
        if(string === null)
            throw "Null string not allowed";
        if(string === ""){
            this.doc_wordCount++;
            return null;
        }

        var index = this.searchChild(string.substring(0,1), SC_MODE_ADD);

        if(index < this.childList.length && this.childList[index].value === string.substring(0,1))
            return this.childList[index].add(string.substring(1));
        else{
            this.childList.splice(index, 0, new TrieNode(string.substring(0,1)));
            return this.childList[index].add(string.substring(1));
        }
    }
    /**
    * returns the total word count of the word word in the trie root
    * @param {*} root 
    * @param {*} word 
    */
    getWordCount(word){
        let wordNode = this.search(word, S_MODE_NODE);
        return wordNode === null? 0 : wordNode.wordCount;
    }

    incrementCount(){
        this.wordCount++;
    }
    incrementDocCount(){
        this.docCount++;
    }
}
exports.TrieNode = TrieNode;
//GLOBAL VARIABLES
exports.g_stopwords = ["a","abord","absolument","afin","ah","ai","aie","ailleurs","ainsi","ait","allaient","allo","allons","allô","alors","anterieur","anterieure",
"anterieures","apres","après","as","assez","attendu","au","aucun","aucune","aujourd","aujourd'hui","aupres","auquel","aura","auraient","aurait","auront","aussi",
"autre","autrefois","autrement","autres","autrui","aux","auxquelles","auxquels","avaient","avais","avait","avant","avec","avoir","avons","ayant","b","bah","bas",
"basee","bat","beau","beaucoup","bien","bigre","boum","bravo","brrr","c","car","ce","ceci","cela","celle","celle-ci","celle-là","celles","celles-ci","celles-là",
"celui","celui-ci","celui-là","cent","cependant","certain","certaine","certaines","certains","certes","ces","cet","cette","ceux","ceux-ci","ceux-là","chacun",
"chacune","chaque","cher","chers","chez","chiche","chut","chère","chères","ci","cinq","cinquantaine","cinquante","cinquantième","cinquième","clac","clic","combien",
"comme","comment","comparable","comparables","compris","concernant","contre","couic","crac","d","da","dans","de","debout","dedans","dehors","deja","delà","depuis",
"dernier","derniere","derriere","derrière","des","desormais","desquelles","desquels","dessous","dessus","deux","deuxième","deuxièmement","devant","devers","devra",
"different","differentes","differents","différent","différente","différentes","différents","dire","directe","directement","dit","dite","dits","divers","diverse",
"diverses","dix","dix-huit","dix-neuf","dix-sept","dixième","doit","doivent","donc","dont","douze","douzième","dring","du","duquel","durant","dès","désormais","e",
"effet","egale","egalement","egales","eh","elle","elle-même","elles","elles-mêmes","en","encore","enfin","entre","envers","environ","es","est","et","etant","etc",
"etre","eu","euh","eux","eux-mêmes","exactement","excepté","extenso","exterieur","f","fais","faisaient","faisant","fait","façon","feront","fi","flac","floc","font",
"g","gens","h","ha","hein","hem","hep","hi","ho","holà","hop","hormis","hors","hou","houp","hue","hui","huit","huitième","hum","hurrah","hé","hélas","i","il","ils",
"importe","j","je","jusqu","jusque","juste","k","l","la","laisser","laquelle","las","le","lequel","les","lesquelles","lesquels","leur","leurs","longtemps","lors",
"lorsque","lui","lui-meme","lui-même","là","lès","m","ma","maint","maintenant","mais","malgre","malgré","maximale","me","meme","memes","merci","mes","mien","mienne",
"miennes","miens","mille","mince","minimale","moi","moi-meme","moi-même","moindres","moins","mon","moyennant","multiple","multiples","même","mêmes","n","na","naturel",
"naturelle","naturelles","ne","neanmoins","necessaire","necessairement","neuf","neuvième","ni","nombreuses","nombreux","non","nos","notamment","notre","nous",
"nous-mêmes","nouveau","nul","néanmoins","nôtre","nôtres","o","oh","ohé","ollé","olé","on","ont","onze","onzième","ore","ou","ouf","ouias","oust","ouste","outre",
"ouvert","ouverte","ouverts","o|","où","p","paf","pan","par","parce","parfois","parle","parlent","parler","parmi","parseme","partant","particulier","particulière",
"particulièrement","pas","passé","pendant","pense","permet","personne","peu","peut","peuvent","peux","pff","pfft","pfut","pif","pire","plein","plouf","plus",
"plusieurs","plutôt","possessif","possessifs","possible","possibles","pouah","pour","pourquoi","pourrais","pourrait","pouvait","prealable","precisement","premier",
"première","premièrement","pres","probable","probante","procedant","proche","près","psitt","pu","puis","puisque","pur","pure","q","qu","quand","quant","quant-à-soi",
"quanta","quarante","quatorze","quatre","quatre-vingt","quatrième","quatrièmement","que","quel","quelconque","quelle","quelles","quelqu'un","quelque","quelques",
"quels","qui","quiconque","quinze","quoi","quoique","r","rare","rarement","rares","relative","relativement","remarquable","rend","rendre","restant","reste","restent",
"restrictif","retour","revoici","revoilà","rien","s","sa","sacrebleu","sait","sans","sapristi","sauf","se","sein","seize","selon","semblable","semblaient","semble",
"semblent","sent","sept","septième","sera","seraient","serait","seront","ses","seul","seule","seulement","si","sien","sienne","siennes","siens","sinon","six","sixième",
"soi","soi-même","soit","soixante","son","sont","sous","souvent","specifique","specifiques","speculatif","stop","strictement","subtiles","suffisant","suffisante",
"suffit","suis","suit","suivant","suivante","suivantes","suivants","suivre","superpose","sur","surtout","t","ta","tac","tant","tardive","te","tel","telle","tellement",
"telles","tels","tenant","tend","tenir","tente","tes","tic","tien","tienne","tiennes","tiens","toc","toi","toi-même","ton","touchant","toujours","tous","tout","toute",
"toutefois","toutes","treize","trente","tres","trois","troisième","troisièmement","trop","très","tsoin","tsouin","tu","té","u","un","une","unes","uniformement",
"unique","uniques","uns","v","va","vais","vas","vers","via","vif","vifs","vingt","vivat","vive","vives","vlan","voici","voilà","vont","vos","votre","vous","vous-mêmes",
"vu","vé","vôtre","vôtres","w","x","y","z","zut","à","â","ça","ès","étaient","étais","était","étant","été","être","ô"];
//MACHINE LEARNING API


exports.init = function (){
    exports.g_stopwords_trie = stringArrayToTrie(exports.g_stopwords);
    exports.g_general_trie = new TrieNode("");
    exports.g_doc_trie_dictionary = new Array();

}

exports.putNewQuestionInTextMining = function (string){
    if(string.length == 1)
        string.replace(/\./g,' ').toLowerCase();
        string = string.split(" ");
    let i;
    let root = new TrieNode("");
    for(i in string)
        if(exports.g_stopwords_trie.search(string[i], S_MODE_BOOLEAN) === true)
            string.splice(i, 1);
    root = stringArrayToTrie(string);
    exports.g_doc_trie_dictionary.push(root);
    mergeRootOntoGeneralRoot(root);
    return string;
}

exports.getNearestNeighbour = function (string){
    text = exports.putNewQuestionInTextMining(string);
    let i;
    let minIndex;
    let minValue = undefined;
    for(i in exports.g_doc_trie_dictionary){
        let j;
        let sum = 0.0;
        for(j in text){
            sum += TFIDF(text[j], i);
        }
        if(sum < minValue || minValue === undefined){
            minValue = sum;
            minIndex = i;
        }
    }
    return minIndex;
}
/**
 * This function will merge the trie whose root is root2 onto the trie whose root is root1.
 * As it is intended to be used to add a document to the general trie, it will increase the doc count
 * for the common words. and add their word count. 
 * @param {TrieNode} root1 node to be merged upon 
 * @param {TrieNode} root2 node to merge onto root1 
 */
function mergeRootOntoGeneralRoot(root){
    if(exports.g_general_trie.childList.length < 1){
        if(root.childList.length < 1){
            exports.g_general_trie.general_wordCount += root.doc_wordCount;
            return;
        }        
    }

    let i;
    for(i in root.childList){
        let firstChar = root.childList[i].value.substring(0,1);
        let index = exports.g_general_trie.searchChild(firstChar, SC_MODE_ADD);
        if(index > exports.g_general_trie.childList.length || exports.g_general_trie.childList[index] !== firstChar){
            root.childList[i].incrementDocCountChildren();
            exports.g_general_trie.childList.splice(index, 0, root.childList[i]);
        }
            
        else
            mergeRootOntoGeneralRoot(exports.g_general_trie.childList[index], root.childList[i]);
    }

}

/**
 * 
 * @param {*} word 
 * @param {*} docKey 
 */
function TF(word, index){
    console.log("size: "+exports.g_doc_trie_dictionary.length+", index: "+index);
    let count = exports.g_doc_trie_dictionary[index].getWordCount(word);
    let totalWordCountInDoc = getTotalWordCountInDoc(exports.g_doc_trie_dictionary[index]);
    let logarg = 1 + (count/totalWordCountInDoc);
    return Math.log10(logarg);
}
/**
 * 
 * @param {*} word 
 */
function IDF(word){
    let docCount = exports.g_doc_trie_dictionary.length;
    let docContainingWordCount = 0;
    let i;
    for(i in exports.g_doc_trie_dictionary)
        if(exports.g_doc_trie_dictionary[i].search(word, S_MODE_BOOLEAN))
            docContainingWordCount++;
    return Math.log10((docCount/docContainingWordCount));
}
/**
 * 
 * @param {*} word 
 * @param {*} docKey 
 */
function TFIDF(word, index){ return TF(word, index)*IDF(word); }
/**
 * counts all the words in the root rie
 * @param {TrieNode} root 
 */
function getTotalWordCountInDoc(root){
    if(root.childList.length < 1)
        return root.wordCount;
    let i;
    let sum = 0;
    for(i in root.childList)
        sum += getTotalWordCountInDoc(root.childList[i]);
    return sum;
}


/**
 * converts an array of string into a trie
 * @param {*} text 
 */
function stringArrayToTrie(text){
    var root = new exports.TrieNode("");
    var i;
    for(i in text)
        root.add(text[i]);
    return root;
}






















































//DEBUT DES FONCTIONS TEST
const STR_ARRAY_TEST_2 = ["java", "javascript", "cplusplus",
                          "cé", "csharp", "assembleur",
                          "mozilla", "mozillafirefox", "chrome"]; 

const STR_ARRAY_TEST_1 = ["araignée", "champignon", "explosif",
                          "physique", "physicien", "physicienne",
                          "informaticien", "informaticienne", "informatique"]; 
const STR_COMMON_TEST_1 = ["array", "array", "array",
                           "linkedlist", "linkedlist", "trie"];
function testCounts(){
    let toMerge = stringArrayToTrie(STR_COMMON_TEST_1);
    let bigroot = stringArrayToTrie(exports.g_stopwords);
    let res = "reussi";

    let arrayCount = toMerge.search("array", S_MODE_NODE).doc_wordCount;
    if(arrayCount != 3) res+=" failed: array count is "+arrayCount+" expected 3";

    let linkedlistCount = toMerge.search("linkedlist", S_MODE_NODE).doc_wordCount;
    if(linkedlistCount != 2) res+=" failed: linkedlist count is "+linkedlistCount+" expected 2";

    mergeRootOntoGeneralRoot(toMerge);
    let i;
    for(i in STR_COMMON_TEST_1){
        let docCount = exports.g_general_trie.search(STR_COMMON_TEST_1[i], S_MODE_NODE).docCount;
        if(docCount != 1) res += " failed: expected word "+STR_COMMON_TEST_1[i]+"'s doc count to be 1, found "+docCount
    }

    mergeRootOntoGeneralRoot(bigroot);
    mergeRootOntoGeneralRoot(bigroot);
    for(i in exports.g_stopwords){
        let docCount = exports.g_general_trie.search(exports.g_stopwords[i], S_MODE_NODE).docCount;
        if(docCount != 2) res += " failed: expected word "+exports.g_stopwords[i]+"'s doc count to be 2, found "+docCount
    }

    document.getElementById("testCounts").innerHTML = res;     
}

function printTrie(){
    var root = new TrieNode("");
    var i;
    for(i in STR_ARRAY_TEST_1)
       root.add(STR_ARRAY_TEST_1[i]);

    var res = new Array();
    putTrieInString(root, res);
    var str = "";
    for(i in res)
        str += " "+res[i];
    
    document.getElementById("printTrie").innerHTML = str;
}

function putTrieInString(root, res){
    res.splice(1,0,root.value);
    var i;
    if(root.childList.length < 1)
        return;
    for(i in root.childList)
        putTrieInString(root.childList[i], res);
}

function testAdd1(){
    var i;
    var root = new TrieNode("");

    for(i in STR_ARRAY_TEST_1)
        root.add(STR_ARRAY_TEST_1[i]);
    var res = "reussi";
    for(i in STR_ARRAY_TEST_1)
        if(root.add(STR_ARRAY_TEST_1[i]) !== null)
            res += " erreur "+STR_ARRAY_TEST_1[i];
    document.getElementById("testAdd1").innerHTML = res;
}

function testAddAndSearch(){
    var res = "reussi";
    var emptyroot = new TrieNode("");
    var root1 = new TrieNode("");
    var root2 = new TrieNode("");
    var i;
    for(i in STR_ARRAY_TEST_1)
        root1.add( STR_ARRAY_TEST_1[i]);
    for(i in STR_ARRAY_TEST_2)
        root2.add( STR_ARRAY_TEST_2[i]);

    for(i in STR_ARRAY_TEST_1)
        if(root1.search( STR_ARRAY_TEST_1[i], S_MODE_BOOLEAN) === false)
            res += "\n test rate cherche ARRAY_1 dans root1: "+STR_ARRAY_TEST_1[i];

    for(i in STR_ARRAY_TEST_2)
        if(root2.search( STR_ARRAY_TEST_2[i], S_MODE_BOOLEAN) === false)
            res += " test rate cherche ARRAY_2 dans root2: "+STR_ARRAY_TEST_2[i];

    for(i in STR_ARRAY_TEST_1)
        if(root2.search( STR_ARRAY_TEST_1[i], S_MODE_BOOLEAN))
            res += " test rate cherche ARRAY_1 dans root2: "+STR_ARRAY_TEST_1[i];
    
    for(i in STR_ARRAY_TEST_2)
        if(root1.search( STR_ARRAY_TEST_2[i], S_MODE_BOOLEAN))
            res += " test rate cherche ARRAY_2 dans root1: "+STR_ARRAY_TEST_2[i];
    for(i in exports.g_stopwords)
        if(exports.g_stopwords_trie.search(exports.g_stopwords[i], S_MODE_BOOLEAN) === false)
            res += " test rate cherche exports.g_stopwords dans stopwords_trie: "+exports.g_stopwords[i];

    document.getElementById("testAddAndSearch").innerHTML = res;            
}
function testMerge(){
    let res = "reussi";
    let root1 = stringArrayToTrie(STR_ARRAY_TEST_1);
    let root2 = stringArrayToTrie(STR_ARRAY_TEST_2);
    let rootBig = stringArrayToTrie(exports.g_stopwords);
    mergeRootOntoGeneralRoot(root2);
    let i;
    for(i in STR_ARRAY_TEST_2)
        if(root2.search(STR_ARRAY_TEST_2[i], S_MODE_BOOLEAN) === false)
            res += " test failed: "+STR_ARRAY_TEST_2[i]+" not found ";
    for(i in STR_ARRAY_TEST_1)
        if(root1.search(STR_ARRAY_TEST_1[i], S_MODE_BOOLEAN) === false)
            res += " test failed: "+STR_ARRAY_TEST_1[i]+" not found ";
    mergeRootOntoGeneralRoot(root2);
    for(i in STR_ARRAY_TEST_2)
        if(exports.g_general_trie.search(STR_ARRAY_TEST_2[i], S_MODE_BOOLEAN) === false)
            res += " test rate cherche ARRAY2 dans emptyroot: "+STR_ARRAY_TEST_2[i];

    mergeRootOntoGeneralRoot(rootBig);
    for(i in exports.g_stopwords)
        if(exports.g_general_trie.search(exports.g_stopwords[i], S_MODE_BOOLEAN) === false)
            res += " test rate cherche stopwords dans emptyroot: "+exports.g_stopwords[i];

    document.getElementById("testMerge").innerHTML = res;
}


function testSearch(root, string){
    res = root.search(string);  
    document.getElementById("demo").innerHTML = res;
}