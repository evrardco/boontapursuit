class Player {

    constructor(id, name, color) {
        this.id = id;
        this.name = name;
        if (color === 'Rouge')
            this.color = '#BF553F';
        else if (color === 'Bleu')
            this.color = '#3333FF';
        else if (color === 'Vert')
            this.color = '#078C5F';
        else
            this.color = '#E6E600';
        this.leftPos = 3;
        this.topPos = 0.75;
        if (this.id === 1) {
            this.leftPos += 2;
        }
        if (this.id === 2)
            this.topPos += 2;
        if (this.id === 3) {
            this.leftPos += 2;
            this.topPos += 2;
        }
        this.token = '<div id="token' + this.id + '" class="token" style="background-color: ' + this.color + ';position: absolute; top: ' + this.topPos + 'vw; left: ' + this.leftPos + 'vw">' + this.name.charAt(0) + '</div>';
        this.distEndOfLine = 9;
        this.currentTileNumber = 0;
        this.currentRow = 0;
    }

    static rollDice() {
        let result = Math.floor(Math.random() * 6 + 1);
        $('#dice').text('Dé : ' + result);
        console.log('Rolled dice : ' + result);
        return result;
    }

    move(length) {
        if (this.currentTileNumber + length > board.tiles.length - 1) {
            var moveAttribute = {left: board.tiles[board.tiles.length - 1].x + this.leftPos + 'vw', top: board.tiles[board.tiles.length - 1].y + this.topPos + 'vw'};
            this.currentTileNumber = board.tiles.length - 1;
        }
        else {
            var moveAttribute = {
                left: board.tiles[this.currentTileNumber + length].x + this.leftPos + 'vw',
                top: board.tiles[this.currentTileNumber + length].y + this.topPos + 'vw'
            };
            this.currentTileNumber += length;
        }
        $('#token' + this.id).animate(moveAttribute, 200);
    }

    draw() {
        $('.plateau').append(this.token);
        console.log('added player ' + this.name);
        this.drawSidePanel();
    }

    drawSidePanel() {
        if (board.currentPlayer.id === this.id)
            $('.sidenav-top').append('<a class="current-playername">> ' + this.name + '</a>');
        else
            $('.sidenav-top').append('<a class="playername">' + this.name + '</a>');
    }

}