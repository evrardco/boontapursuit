class Board {

    constructor(tiles, players) {
        this.tiles = tiles;
        this.players = players;
        this._currentPlayer = 0;
        $('.questionbar').append('<a class="question">Lancez le dé !</a>');
    }

    nextPlayer() {
        if (this._currentPlayer === this.players.length - 1)
            this._currentPlayer = 0;
        else
            this._currentPlayer = this._currentPlayer + 1;
        $('.sidenav-top').empty();
        for (let i = 0; i < this.players.length; i++) {
            this.players[i].drawSidePanel();
        }
        $('.questionbar').empty();
        $('.questionbar').append('<a class="question">Lancez le dé !</a>');
        $('#dice-button-disabled').attr('id', 'dice-button');
        $('#endTurn-button').attr('id', 'endTurn-button-disabled');
    }

    makeTurn() {
        this.currentPlayer.move(Player.rollDice());
        this.getQuestion(this.currentPlayer.currentTileNumber);
        $('#dice-button').attr('id', 'dice-button-disabled');
    }

    gameOver(winnerPlayer) {
        console.log('Game over ! Winner is ' + this.currentPlayer);
        $('.questionbar').empty();
        $('.questionbar').append('<a class="question">Le vainqueur est ' + this.currentPlayer.name + ' !</a>');
        $('#dice-button').attr('id', 'dice-button-disabled');
    }

    getQuestion(tileNumber) {
        $.ajax({
                type : 'GET',
                url: 'queryQuestion',
                dataType: 'json',
                asynx: false,
                data: {type: this.tiles[tileNumber].type},
                success: function(result) {
                    board.currentQuestion = result;
                    $('.questionbar').empty();
                    $('.questionbar').append('<a class="question">' + board.currentQuestion.Question + '</a>');
                    for (let i = 0; i < board.currentQuestion.Choices.length; i++) {
                        $('.questionbar').append('<a id="answer' + i + '" class="answer" href="#" onclick="board.selectAnswer(' + i + ')">' + board.currentQuestion.Choices[i].text + '</a>');
                    }
                },
                error: function(xhr, status, error) {
                    alert(error);
                }
            });
    };
    set currentPlayer(player) {
        this._currentPlayer = player;
    }

    get currentPlayer() {
        return this.players[this._currentPlayer];
    }

    selectAnswer(id) {
        $('.questionbar').empty();
        if (this.currentQuestion.Choices[id].isValid) {
            console.log('Bonne réponse');
            $('.questionbar').append('<a class="question">Bonne réponse ! :D</a>');
            if (this.currentPlayer.currentTileNumber === this.tiles.length - 1)
                this.gameOver(this.currentPlayer);
            else {
                this.currentPlayer.move(Player.rollDice());
                $('#endTurn-button-disabled').attr('id', 'endTurn-button');
            }
        } else {
            console.log('Mauvaise réponse');
            $('.questionbar').append('<a class="question">Mauvaise réponse ! D:</a>');
            $('#endTurn-button-disabled').attr('id', 'endTurn-button');
        }
    }
    
    draw() {
        let j = 0, i = 0, turn = 0;
        let invert = false;
        for (let count = 0; count < this.tiles.length; count++) {
            let tileClass = 'tile';
            if (count == 0) tileClass += '-first';
            else if (count == this.tiles.length - 1) tileClass += '-last';
            else if (count == 10 + turn - 1) tileClass += '-lastofline';
            else if (count == 10 + turn) {
                tileClass += '-firstofline';
                invert = !invert;
            }
            if (invert) {
                tileClass += '-inverted';
                var textClass = 'text-inverted';
            } else var textClass = 'text';
            tileClass += ' ' + this.tiles[count].color; // couleurs
            if (count == 10 + turn) {
                j++;
                var tile = '<a class="' + tileClass + '"> <div class="' + textClass + '">' + this.tiles[count].type + '</div> <div class="arrow"></div> </a>';
                container = '<div class="tile-container" style=" z-index: ' + -count + '; position: relative; top: ' + j * 10 + 'vw; left:' + i * 8 + 'vw">' + tile + '</div>';
                this.tiles[count].x = i * 8;
                this.tiles[count].y = j * 10;
                turn += 10;
            } else {
                if (count == (10 + turn) - 1) var div = '<div class="arrow"></div>';
                else var div = '';
                if (count != 0)
                    invert ? i-- : i++;
                var tile = '<a class="' + tileClass + '"> <div class="' + textClass + '">' + this.tiles[count].type + '</div> ' + div + '</a>';
                var container = '<div class="tile-container" style=" z-index: ' + -count + '; position: relative; top: ' + j * 10 + 'vw; left:' + i * 8 + 'vw">' + tile + '</div>';
                this.tiles[count].x = i * 8;
                this.tiles[count].y = j * 10;
            }
            $(".plateau").append(container);
        }
        // draw players
        for (let i = 0; i < this.players.length; i++) {
            this.players[i].draw();
        }
    }

}