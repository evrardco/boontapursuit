var express = require('express');
var path = require('path');
var engines = require('consolidate');
var MongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
var bodyParser = require('body-parser')
var app = express();
var TextMiner = require("./static/js/TextMining.js");

parsedData = '';

app.engine('html', engines.hogan);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, 'static')));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

MongoClient.connect('mongodb://localhost:27017', (err, db) => {
	dbo = db.db("boontapursuit");
  if(err) throw err;
  console.log('Connecté à la base de données !');
  dbo.collection("questions").find({}).toArray(function(err, result) { //find all elements of the collection
    if (err) throw err;
    TextMiner.init();
    let i;
    for(i in result){
      TextMiner.putNewQuestionInTextMining(result[i].Question);
    }
    console.log(result);
  });
  

  app.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname, '/static/startPage.html'));
  });

  app.get('/addDB.html', function (req, res, next) {
    res.render('addDB.html', {toshow: ""});
  });

  app.post('/addDB.html', function (req, res, next) { //pour ajouter une question a la base de donnee
    var q = req.body;
    var message = "";
    if (q.category === 'Auto') {
      dbo.collection("questions").find({}).toArray(function(err, result) { //find all elements of the collection
          if (err) throw err;
          let index = TextMiner.getNearestNeighbour(q.question); //pour detecter la categorie
          dbo.collection("questions").insertOne({"Question": q.question, "Category": result[index].Category, "Choices": q.choices}, function(err, res) {
            if (err) throw err;
            console.log("1 document inséré !");
            console.log("Categorie devinée pour: "+q.question+": "+result[index].Category);
          });
        console.log(result);
        });
    } else {
      if (formatDBInput(q)) { //verifie si la question est valide
        dbo.collection("questions").insertOne({"Question": q.question, "Category": q.category, "Choices": q.choices}, function(err, res) {
          if (err) throw err;
          console.log("1 document inséré !");
          console.log(q);
          message = "Question inséré !";
        });
      } else {
        console.log("Question incomplète !");
        console.log(q);
        message = "Question incomplète !";
      }
    }
    res.render('addDB.html', {toshow: message});
  });

  app.post('/showDB.html', function (req, res, next) { //pour retirer des questions
    var q = req.body;
    var count = 0;
    if (q.remove !== undefined && q.remove.constructor === Array) { //variable.constructor === Array --> regarde si 'variable' est un Array
      q.remove.forEach(element => {
        dbo.collection("questions").deleteOne({_id: ObjectId(element)}, function(err, result) {
          if (err) throw err;
        });
        count++;
      });
    } else {
      dbo.collection("questions").deleteOne({_id: ObjectId(q.remove)}, function(err, result) {
        if (err) throw err;
      });
      count++;
    }
    console.log(count + " document(s) supprimé(s) !");
    res.redirect('back');
  });

  app.get('/showDB.html', function (req, res, next) { //pour montrer les questions
    var q = req.query;
    if (q.show === undefined || q.show === 'All') {
      dbo.collection("questions").find({}).toArray(function(err, result) { //find all elements of the collection
        if (err) throw err;
        console.log(result);
        res.render('showDB.html', formatDBOutput(result));
      });
    } else {
      dbo.collection("questions").find({Category: q.value}).toArray(function(err, result) {
        if (err) throw err;
        console.log(q);
        res.render('showDB.html', formatDBOutput(result));
      });
    }
  });
  
  app.get('/queryQuestion', function (req, res, next) { //envoie une question au hasard de la collection pour pouvoir jouer
    var data = req.query;
    if (data.type === 'Arrivée') {
      dbo.collection("questions").find({}).toArray(function(err, result) {
        if (err) throw err;
        res.send(result[Math.floor(Math.random()*result.length)]);
      });
    } else {
      dbo.collection("questions").find({Category: data.type}).toArray(function(err, result) {
        if (err) throw err;
        res.send(result[Math.floor(Math.random()*result.length)]);
      });
    }
  });

  app.get('/queryPlayers', function (req, res, next) { //recupere les donnees des joueurs qui ont cree lors de la creation de la partie
    res.send(parsedData);
  });

  app.post('/game', function (req, res, next) {
    var data = req.body;
    //console.log(data);
    parsedData = parsePlayersData(data);
    res.sendFile(path.join(__dirname, '/static/gameBoard.html'));
  });
  
  app.use(function(req, res, next) { 
    res.status(404).sendFile(path.join(__dirname, '/static/notFound.html'));
  });
  
  app.listen(8080);
  console.log('Express lancé sur le port 8080 !');
});

function parsePlayersData(playersData) { //formatte les donnes recuperes lors de la creation de la partie
  for (let i = 0; i < playersData.players.length; i++) {
    if (playersData.players[i] == '') {
      let suffix = 'ème';
      if (i === 0)
          suffix = 'er'
      playersData.players[i] = (i + 1) + suffix + ' joueur';
    }
  }
  return playersData;
}

/*
 * Formatte le résultat de la recherche dans la base de donnée en un format utilisable par le moteur de rendu Hogan
 */
function formatDBOutput(dbOutput) {
  for (let i = 0; i < dbOutput.length; i++) {
    if (dbOutput[i].Choices.length < 4) { // moche mais pour le rendu avec hogan necessaire sinon le tableau n'est pas bien formaté
      for (let j = 0; j <= 4 - dbOutput[i].Choices.length; j++) {
        dbOutput[i].Choices.push({});
      }
    }
  }
  return {toshow: dbOutput};
}

/*
 * Formatte les données envoyées par l'application en un format à entrer dans la base de données
 * retourne false si les données reçues ne sont pas conformes, true sinon
 */
function formatDBInput(dbInput) {
  if (dbInput.question === '' || dbInput.isvalid === undefined) // on regarde si une question a été posée et s'il y a au moins une bonne réponse
    return false;
  if (dbInput.isvalid.constructor !== Array)
    dbInput.isvalid = [dbInput.isvalid];
  
  var isValidTemp = [false, false, false, false];
  for (let i = 0; i < dbInput.isvalid.length; i++) {
    if (dbInput.choices[dbInput.isvalid[i]] == '') // on empêche les cas où un bon choix est lié à un choix vide
      return false;
    isValidTemp[dbInput.isvalid[i]] = true;
  }
  var Choices = [];
  for (let i = 0; i < dbInput.choices.length; i++) { // on enlève les entrées vides du tableau de choix
    if (dbInput.choices[i] !== '')
      Choices.push({"text": dbInput.choices[i], "isValid": isValidTemp[i]});
  }
  if (Choices.length < 2) // on vérifie qu'il y a un minimum de 2 choix
    return false;
  console.log(Choices);
  dbInput.choices = Choices; // on remplace les choix reçus par les choix bien formatés
  return true;
}
