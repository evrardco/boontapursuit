export class Player {

    constructor(id, name, color) {
        this.id = id;
        this.name = name;
        if (color === 'Rouge')
            this.color = '#BF553F';
        else if (color === 'Bleu')
            this.color = '#3333FF';
        else if (color === 'Vert')
            this.color = '#078C5F';
        else
            this.color = '#E6E600';
        this.distEndOfLine = 9;
        this.currentTileNumber = 0;
        this.currentRow = 0;
        this.maxlength = 30;
    }

    rollDice() {
        let result = Math.floor(Math.random() * 6 + 1);
        return result;
    }

    move(length) {
        if (this.currentTileNumber + length > this.maxlength - 1)
            this.currentTileNumber = this.maxlength - 1;
        else
            this.currentTileNumber += length;
    }

}