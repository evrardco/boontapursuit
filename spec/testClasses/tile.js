export class Tile {
    constructor(type) {
        this.type = type;
        if (type === 'Histoire')
            this.color = 'green';
        else if (type === 'Géographie')
            this.color = 'pink';
        else if (type === 'Culture')
            this.color = 'red';
        else if (type === 'Science')
            this.color = 'blue';
        else if (type === 'Départ')
            this.color = 'purple';
        else if (type === 'Arrivée')
            this.color = 'orange';
    }
}
