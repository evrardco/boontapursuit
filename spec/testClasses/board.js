export class Board {

    constructor(tiles, players) {
        this.tiles = tiles;
        this.players = players;
        this._currentPlayer = 0;
    }

    nextPlayer() {
        if (this._currentPlayer === this.players.length - 1)
            this._currentPlayer = 0;
        else
            this._currentPlayer = this._currentPlayer + 1;
    }

    makeTurn() {
        this.currentPlayer.move(Player.rollDice());
    }

    set currentPlayer(player) {
        this._currentPlayer = player;
    }

    get currentPlayer() {
        return this.players[this._currentPlayer];
    }

}