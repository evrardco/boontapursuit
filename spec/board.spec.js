import { Player } from './testClasses/player';
import { Board } from './testClasses/board';
import { Tile } from './testClasses/tile';
 
describe('when a game is created', () => {
    let board;
    let players;
    let tiles;
    beforeEach(() => {
        tiles = [
            new Tile('Départ'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'),
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'), 
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'), 
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'),
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'),
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'),
            new Tile('Histoire'), new Tile('Géographie'), new Tile('Culture'), new Tile('Science'),
            new Tile('Histoire'), new Tile('Arrivée')
        ];
        players = [new Player(0, 'Colin', 'Rouge'), new Player(1, 'Maxime', 'Bleu'), new Player(2, 'Jean-François', 'Jaune')];
        board = new Board(tiles, players);
    });
 
    it('current player should be the first one', () => {
        expect(board.currentPlayer.name).toEqual('Colin');
    });

    it('all players should be at the first tile', () => {
        for (let i = 0; i < board.players.lenght; i++) {
            expect(board.currentPlayer.currentTileNumber.toEqual(0));
        }
    });

    describe('when player ends its turn', () => {
        it('the current player should be different', () => {
            expect(board.currentPlayer.name).toBe('Colin');
            board.nextPlayer();
            expect(board.currentPlayer.name).toEqual('Maxime');
        });
    });
 
    describe('when player rolls the dice', () => {
        it('should move the ammount of tiles shown by the dice', () =>{
            let tileNumber = board.currentPlayer.currentTileNumber;
            let diceValue = board.currentPlayer.rollDice();
            board.currentPlayer.move(diceValue);
            expect(board.currentPlayer.currentTileNumber).toBeGreaterThan(tileNumber);
            expect(board.currentPlayer.currentTileNumber).toEqual(tileNumber + diceValue);
        }); 
    });
 
    describe('when goes beyond the game limits', () => {
        beforeEach( () => {
            board.currentPlayer.move(100);
        });
 
        it('should move to the end', () => {
            expect(board.currentPlayer.currentTileNumber).toEqual(board.tiles.length - 1);
        });
    }); 

});